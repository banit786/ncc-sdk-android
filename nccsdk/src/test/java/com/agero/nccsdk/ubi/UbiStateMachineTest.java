package com.agero.nccsdk.ubi;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.Field;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UbiStateMachineTest {

    private UbiStateMachine ubiStateMachine;

    @Mock
    private UbiState mockUbiState;

    @Before
    public void setUp() throws Exception {
        ubiStateMachine = new UbiStateMachine(mockUbiState);
    }

    @Test
    public void initWithState() {
        assertTrue(getState(ubiStateMachine) instanceof UbiState);
    }

    @Test
    public void onDrivingStart() {
        ubiStateMachine.onDrivingStart();
        verify(mockUbiState).startCollecting(ubiStateMachine);
    }

    @Test
    public void onDrivingStop() {
        ubiStateMachine.onDrivingStop();
        verify(mockUbiState).stopCollecting(ubiStateMachine);
    }

    private Object getState(UbiStateMachine ubiStateMachine) {
        try {
            Field f = getField(UbiStateMachine.class, "currentState");
            f.setAccessible(true);
            f.get(ubiStateMachine);

            return f.get(ubiStateMachine);
        } catch (NoSuchFieldException nsfe) {
            nsfe.printStackTrace();
        } catch (IllegalAccessException iae) {
            iae.printStackTrace();
        }

        return null;
    }

    private Field getField(Class clazz, String fieldName) throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
}
