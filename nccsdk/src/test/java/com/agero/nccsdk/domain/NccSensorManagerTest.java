package com.agero.nccsdk.domain;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccAccelerometerConfig;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.config.NccLocationConfig;
import com.agero.nccsdk.domain.data.NccSensorData;
import com.agero.nccsdk.domain.sensor.NccAbstractSensor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by james hermida on 11/15/17.
 */

@RunWith(PowerMockRunner.class)
public class NccSensorManagerTest {

    private NccSensorManager nccSensorManager;

    private NccSensorType testSensorType = NccSensorType.ACCELEROMETER;
    private NccSensorListener testSensorListener = new NccSensorListener() {
        @Override
        public void onDataChanged(NccSensorType sensorType, NccSensorData data) {

        }
    };
    private NccConfig testSensorConfig = new NccAccelerometerConfig();

    @Mock
    private PackageManager mockPackageManager;

    @Mock
    private Context mockContext;

    @Before
    public void setUp() throws Exception {
        nccSensorManager = PowerMockito.spy(new NccSensorManager(mockContext));
    }

    @Test
    @PrepareForTest({NccSensorManager.class})
    public void subscribeSensorListener() throws Exception {
        mockIsSensorAvailable(true);

        android.hardware.SensorManager mockSensorManager = mock(android.hardware.SensorManager.class);
        when(mockContext.getSystemService(Context.SENSOR_SERVICE)).thenReturn(mockSensorManager);

        Sensor mockSensor = mock(Sensor.class);
        when(mockSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)).thenReturn(mockSensor);

        NccAbstractSensor mockAbstractSensor = mock(NccAbstractSensor.class);
        when(nccSensorManager, "getSensor", testSensorType).thenReturn(mockAbstractSensor);

        nccSensorManager.subscribeSensorListener(testSensorType, testSensorListener, testSensorConfig);

        verify(mockAbstractSensor).subscribe(testSensorListener);
        verify(mockAbstractSensor).startStreaming();
    }

    @Test
    @PrepareForTest({NccSensorManager.class})
    public void subscribeSensorListener_sensor_already_streaming() throws Exception {
        mockIsSensorAvailable(true);

        android.hardware.SensorManager mockSensorManager = mock(android.hardware.SensorManager.class);
        when(mockContext.getSystemService(Context.SENSOR_SERVICE)).thenReturn(mockSensorManager);

        Sensor mockSensor = mock(Sensor.class);
        when(mockSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)).thenReturn(mockSensor);

        NccAbstractSensor mockAbstractSensor = mock(NccAbstractSensor.class);
        when(nccSensorManager, "getSensor", testSensorType).thenReturn(mockAbstractSensor);

        when(mockAbstractSensor.isStreaming()).thenReturn(true);

        nccSensorManager.subscribeSensorListener(testSensorType, testSensorListener, testSensorConfig);

        verify(mockAbstractSensor).subscribe(testSensorListener);
        verify(mockAbstractSensor, times(0)).startStreaming();
    }

    @Test
    @PrepareForTest({NccSensorManager.class})
    public void subscribeSensorListener_sensor_unavailable() throws Exception {
        mockIsSensorAvailable(false);

        try {
            nccSensorManager.subscribeSensorListener(testSensorType, testSensorListener, testSensorConfig);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
        }
    }

    @Test
    @PrepareForTest({NccSensorManager.class})
    public void subscribeSensorListener_null_listener() throws Exception {
        mockIsSensorAvailable(true);

        try {
            nccSensorManager.subscribeSensorListener(testSensorType, null, testSensorConfig);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
        }
    }

    @Test
    @PrepareForTest({NccSensorManager.class})
    public void subscribeSensorListener_null_config() throws Exception {
        mockIsSensorAvailable(true);

        try {
            nccSensorManager.subscribeSensorListener(testSensorType, testSensorListener, null);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
        }
    }

    @Test
    @PrepareForTest({NccSensorManager.class})
    public void subscribeSensorListener_config_sensortype_mismatch() throws Exception {
        mockIsSensorAvailable(true);
        NccLocationConfig mismatchedConfig = new NccLocationConfig();

        try {
            nccSensorManager.subscribeSensorListener(testSensorType, testSensorListener, mismatchedConfig);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
        }
    }

    private void mockIsSensorAvailable(boolean available) throws Exception {
        //PowerMockito.when(mockContext, "getPackageManager").thenReturn(mockPackageManager);
        //PowerMockito.when(mockPackageManager, "hasSystemFeature", anyString()).thenReturn(available);

        PowerMockito.doReturn(available).when(nccSensorManager, "isSensorAvailable", testSensorType);
    }

    @Test
    @PrepareForTest({NccSensorManager.class})
    public void unsubscribeSensorListener_with_remaining_subscribers() throws Exception {
        mockIsSensorRegistered(true);

        NccAbstractSensor mockAbstractSensor = mock(NccAbstractSensor.class);
        when(nccSensorManager, "getSensor", testSensorType).thenReturn(mockAbstractSensor);

        when(mockAbstractSensor.hasSubscribers()).thenReturn(true);

        nccSensorManager.unsubscribeSensorListener(testSensorType, testSensorListener);

        verify(mockAbstractSensor).unsubscribe(testSensorListener);
        verify(mockAbstractSensor, times(0)).stopStreaming();
    }

    @Test
    @PrepareForTest({NccSensorManager.class})
    public void unsubscribeSensorListener_with_no_remaining_subscribers() throws Exception {
        mockIsSensorRegistered(true);

        NccAbstractSensor mockAbstractSensor = mock(NccAbstractSensor.class);
        when(nccSensorManager, "getSensor", testSensorType).thenReturn(mockAbstractSensor);

        when(mockAbstractSensor.hasSubscribers()).thenReturn(false);

        nccSensorManager.unsubscribeSensorListener(testSensorType, testSensorListener);

        verify(mockAbstractSensor).unsubscribe(testSensorListener);
        verify(mockAbstractSensor).stopStreaming();
    }

    @Test
    public void unsubscribeSensorListener_null_listener() throws Exception {
        try {
            nccSensorManager.unsubscribeSensorListener(testSensorType, null);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
        }
    }

    @Test
    @PrepareForTest({NccSensorManager.class})
    public void unsubscribeSensorListener_sensor_not_already_registered() throws Exception {
        mockIsSensorRegistered(false);

        try {
            nccSensorManager.unsubscribeSensorListener(testSensorType, testSensorListener);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
        }
    }

    private void mockIsSensorRegistered(boolean registered) throws Exception {
        PowerMockito.doReturn(registered).when(nccSensorManager, "isSensorRegistered", testSensorType);
    }

    @Test
    @PrepareForTest({NccSensorManager.class})
    public void setConfig() throws Exception {
        mockIsSensorRegistered(true);

        NccAbstractSensor mockAbstractSensor = mock(NccAbstractSensor.class);
        when(nccSensorManager, "getSensor", testSensorType).thenReturn(mockAbstractSensor);

        nccSensorManager.setConfig(testSensorType, testSensorConfig);

        verify(mockAbstractSensor).setConfig(testSensorConfig);
    }

    @Test
    public void setConfig_null_config() throws Exception {
        try {
            nccSensorManager.setConfig(testSensorType, null);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
        }
    }

    @Test
    public void setConfig_config_sensortype_mismatch() throws Exception {
        NccLocationConfig mismatchedConfig = new NccLocationConfig();

        try {
            nccSensorManager.setConfig(testSensorType, mismatchedConfig);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
        }
    }

    @Test
    public void setConfig_sensor_not_already_registered() throws Exception {
        try {
            nccSensorManager.setConfig(testSensorType, testSensorConfig);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
        }
    }

    @Test
    @PrepareForTest({NccSensorManager.class})
    public void getConfig() throws Exception {
        mockIsSensorRegistered(true);

        NccAbstractSensor mockAbstractSensor = mock(NccAbstractSensor.class);
        when(nccSensorManager, "getSensor", testSensorType).thenReturn(mockAbstractSensor);

        nccSensorManager.getConfig(testSensorType);
        verify(mockAbstractSensor).getConfig();
    }

    @Test
    public void getConfig_sensor_not_already_registered() throws Exception {
        try {
            nccSensorManager.getConfig(testSensorType);
        } catch (Exception e) {
            assertTrue(e instanceof NccException);
        }
    }
}
