package com.agero.nccsdk.domain.sensor;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import com.agero.nccsdk.NccPermissionsManager;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.config.NccLocationConfig;
import com.agero.nccsdk.domain.data.NccLocationData;
import com.agero.nccsdk.internal.log.Timber;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;

/**
 * Created by james hermida on 8/16/17.
 */

public class NccLocation extends NccAbstractSensor {

    private final FusedLocationProviderClient fusedLocationClient;
    private LocationRequest locationRequest;
    private final OnCompleteListener<Void> startListener;
    private final OnCompleteListener<Void> stopListener;
    private final LocationCallback locationCallback;

    private final Intent serviceIntent;
    private Location lastLocation;

    private HandlerThread handlerThread;
    
    /**
     * Creates an instance of NccLocation
     *
     * @param context The Context allowing access to application-specific resources and classes, as well as
     * up-calls for application-level operations such as launching activities,
     * broadcasting and receiving intents, etc.
     */
    public NccLocation(final Context context, NccConfig config) {
        super(context, NccSensorType.LOCATION, config);
        serviceIntent = new Intent(applicationContext, ForegroundService.class);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);

        startListener = task -> {
            if (task.isSuccessful()) {
                isStreaming = true;

                // Start foreground service for Android O devices
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    applicationContext.startForegroundService(serviceIntent);
                }
            } else {
                if (task.getException() == null) {
                    Timber.e("Request for starting location updates failed. Task did not contain an exception.");
                } else {
                    Timber.e(task.getException(), "Exception starting location updates");
                }
            }
        };

        stopListener = task -> {
            if (task.isSuccessful()) {
                isStreaming = false;

                // Stop foreground service for Android O devices
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    applicationContext.stopService(serviceIntent);
                }
            } else {
                if (task.getException() == null) {
                    Timber.e("Request for stopping location updates failed. Task did not contain an exception.");
                } else {
                    Timber.e(task.getException(), "Exception stopping location updates");
                }
            }
        };

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    Timber.w("locationResult is null");
                } else {
                    for (Location location : locationResult.getLocations()) {
                        // Get distance from last location
                        double distanceFromLastLocation = 0;
                        if (lastLocation != null) {
                            distanceFromLastLocation = location.distanceTo(lastLocation);
                        }

                        NccLocationData nccLocationData = new NccLocationData(location.getTime(), location.getLatitude(), location.getLongitude(),
                                location.getAltitude(), location.getBearing(), location.getAccuracy(), location.getSpeed(), distanceFromLastLocation);

                        postData(nccLocationData);

                        lastLocation = location;
                    }
                }
            }
        };
    }

    /**
     * See {@link NccSensor#startStreaming()}
     */
    @SuppressWarnings({"MissingPermission"})
    @Override
    public void startStreaming() {
        if (!isStreaming()) {
            if (NccPermissionsManager.isPermissionAllowed(Manifest.permission.ACCESS_FINE_LOCATION)) {
                createLocationRequestIfNeeded();
                fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, getLooper())
                        .addOnCompleteListener(startListener);
            } else {
                Timber.e("Unable to start location updates. %s must be granted.", Manifest.permission.ACCESS_FINE_LOCATION);
            }
        }
    }
    
    private Looper getLooper() {
        if (handlerThread == null || !handlerThread.isAlive()) {
            handlerThread = new HandlerThread(NccLocation.class.getSimpleName() + HandlerThread.class.getSimpleName());
            handlerThread.start();
        }
        return handlerThread.getLooper();
    }

    /**
     * See {@link NccSensor#stopStreaming()}
     */
    @Override
    public void stopStreaming() {
        if (isStreaming()) {
            fusedLocationClient.removeLocationUpdates(locationCallback)
                    .addOnCompleteListener(stopListener);
            stopHandlers();
        }
    }

    private void stopHandlers() {
        if (handlerThread != null && handlerThread.isAlive()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                Timber.v("%s sensor event handler thread quit safely result: %s", sensorType.getName(), handlerThread.quitSafely());
            } else {
                Timber.v("%s sensor event handler thread quit result: %s", sensorType.getName(), handlerThread.quit());
            }
        }
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    private void createLocationRequestIfNeeded() {
        if (locationRequest == null) {
            NccLocationConfig config = (NccLocationConfig) this.config;

            locationRequest = new LocationRequest();

            // Sets the desired interval for active location updates. This interval is
            // inexact. You may not receive updates at all if no location sources are available, or
            // you may receive them slower than requested. You may also receive updates faster than
            // requested if other applications are requesting location at a faster interval.
            locationRequest.setInterval(config.getInterval());

            // Sets the fastest rate for active location updates. This interval is exact, and your
            // application will never receive updates faster than this value.
            locationRequest.setFastestInterval(config.getFastestInterval());

            locationRequest.setPriority(config.getPriority());
        }
    }
}
