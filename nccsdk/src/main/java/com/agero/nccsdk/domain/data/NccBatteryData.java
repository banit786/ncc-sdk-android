package com.agero.nccsdk.domain.data;

import java.util.Locale;

/**
 * Created by james hermida on 8/24/17.
 */

public class NccBatteryData extends NccAbstractSensorData {

    private final int level;

    /**
     * The charging state of the device – true if charging, otherwise false
     */
    private final boolean isCharging;

    /**
     * Creates an instance of NccBatteryData
     *
     * @param timestamp UTC timestamp in milliseconds
     * @param level The battery level of the device from 0-100
     * @param isCharging true if the device is charging, otherwise false
     */
    public NccBatteryData(long timestamp, int level, boolean isCharging) {
        super(timestamp);
        this.level = level;
        this.isCharging = isCharging;
    }

    /**
     * Gets the battery level
     *
     * @return int of the battery level from 0 - 100
     */
    public int getLevel() {
        return level;
    }

    /**
     * Gets the charging state
     *
     * @return true if the device is charging, otherwise false
     */
    public boolean isCharging() {
        return isCharging;
    }

    @Override
    public String toString() {
        return "NccBatteryData{" +
                "level=" + level +
                ", isCharging=" + isCharging +
                ", timestamp=" + timestamp +
                '}';
    }
}
