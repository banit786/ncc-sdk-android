package com.agero.nccsdk.lbt.util;

import com.agero.nccsdk.domain.data.NccSensorData;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class DataUtils {

    private static final Gson gson = new Gson();

    public static String toJSONArrayString (List<? extends NccSensorData> list) throws JSONException {
        return toJSONArray(list).toString();
    }

    private static JSONArray toJSONArray(List<? extends NccSensorData> list) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (NccSensorData data : list) {
            JSONObject jsonObject = new JSONObject(gson.toJson(data));
            jsonArray.put(jsonObject);
        }

        return jsonArray;
    }

}
