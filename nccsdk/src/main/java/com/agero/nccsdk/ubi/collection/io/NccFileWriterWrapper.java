package com.agero.nccsdk.ubi.collection.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Writes text to the file immediately after each call to {@link #appendLine(String)}
 */
public class NccFileWriterWrapper extends AbstractFileWriterWrapper {

    private final File file;

    public NccFileWriterWrapper(File file, Headers headers) throws IOException {
        super(new FileWriter(file.getAbsolutePath()), headers);
        this.file = file;
    }

    @Override
    public void appendLine(String s) throws IOException {
        super.appendLine(s);
        writer.flush();
    }

    @Override
    public File getFile() {
        return file;
    }
}
