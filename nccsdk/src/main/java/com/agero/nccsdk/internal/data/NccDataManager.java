package com.agero.nccsdk.internal.data;

import com.agero.nccsdk.TrackingContext;
import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.internal.common.statemachine.SdkConfig;
import com.agero.nccsdk.internal.common.statemachine.SdkConfigType;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.agero.nccsdk.internal.data.memory.ClientCache;
import com.agero.nccsdk.internal.data.preferences.ConfigManager;
import com.agero.nccsdk.internal.data.preferences.SharedPrefs;
import com.agero.nccsdk.internal.notification.model.NccNotification;
import com.agero.nccsdk.adt.detection.start.model.WifiActivity;

import java.security.InvalidParameterException;

import javax.inject.Inject;

/**
 * Created by james hermida on 10/4/17.
 *
 * A facade for handling data that is stored/retrieved to/from memory/disk
 *
 */

public class NccDataManager implements DataManager {

    private final ConfigManager configManager;
    private final SharedPrefs sharedPrefs;
    private final ClientCache clientCache;

    @Inject
    NccDataManager(ConfigManager configManager, SharedPrefs sharedPrefs, ClientCache clientCache) {
        this.configManager = configManager;
        this.sharedPrefs = sharedPrefs;
        this.clientCache = clientCache;
    }

    @Override
    public void saveConfig(NccSensorType sensorType, NccConfig config) {
        if (config == null) {
            throw new InvalidParameterException("config is null");
        }

        configManager.saveConfig(sensorType, config);
    }

    @Override
    public NccConfig getConfig(NccSensorType sensorType) throws NccException {
        return configManager.getConfig(sensorType);
    }

    @Override
    public void saveSdkConfig(SdkConfigType configType, SdkConfig config) {
        if (config == null) {
            throw new InvalidParameterException("config is null");
        }

        configManager.saveSdkConfig(configType, config);
    }

    @Override
    public SdkConfig getSdkConfig(SdkConfigType configType) throws NccException {
        return configManager.getSdkConfig(configType);
    }

    @Override
    public void setUserId(String userId) {
        if (StringUtils.isNullOrEmpty(userId)) {
            throw new InvalidParameterException("userId cannot be null or empty");
        }

        sharedPrefs.setUserId(userId);
    }

    @Override
    public String getUserId() {
        return sharedPrefs.getUserId();
    }

    @Override
    public void setTrackingContext(TrackingContext trackingContext) {
        if (trackingContext == null) {
            throw new InvalidParameterException("trackingContext cannot be null");
        }

        clientCache.setTrackingContext(trackingContext);
    }

    @Override
    public TrackingContext getTrackingContext() {
        return clientCache.getTrackingContext();
    }

    @Override
    public void clearTrackingContext() {
        clientCache.clearTrackingContext();
    }

    @Override
    public void setRecentWifiActivity(WifiActivity recentWifiActivity) {
        if (recentWifiActivity == null) {
            throw new InvalidParameterException("recentWifiActivity cannot be null");
        }

        sharedPrefs.setRecentWifiActivity(recentWifiActivity);
    }

    @Override
    public WifiActivity getRecentWifiActivity() {
        return sharedPrefs.getRecentWifiActivity();
    }

    @Override
    public void saveNotification(NccNotification notification) {
        if (notification == null) {
            throw new InvalidParameterException("notification cannot be null");
        }

        sharedPrefs.saveNotification(notification);
    }

    @Override
    public NccNotification getNotification(String id) {
        return sharedPrefs.getNotification(id);
    }

    @Override
    public void setLbtAutoRestart(boolean bool) {
        sharedPrefs.setLbtAutoRestart(bool);
    }

    @Override
    public boolean shouldLbtAutoRestart() {
        return sharedPrefs.shouldLbtAutoRestart();
    }

    @Override
    public void setAuthenticatedApiKey(String apiKey) {
        if (StringUtils.isNullOrEmpty(apiKey)) {
            throw new InvalidParameterException("apiKey cannot be null or empty");
        }

        sharedPrefs.setAuthenticatedApiKey(apiKey);
    }

    @Override
    public String getAuthenticatedApiKey() {
        return sharedPrefs.getAuthenticatedApiKey();
    }

    @Override
    public void setAccessId(String accessId) {
        if (StringUtils.isNullOrEmpty(accessId)) {
            throw new InvalidParameterException("accessId cannot be null or empty");
        }

        sharedPrefs.setAccessId(accessId);
    }

    @Override
    public String getAccessId() {
        return sharedPrefs.getAccessId();
    }
}
