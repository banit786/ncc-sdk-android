package com.agero.nccsdk.internal.common.io;

import org.apache.commons.compress.archivers.ArchiveException;

import java.io.File;
import java.io.IOException;

/**
 * Created by james hermida on 10/30/17.
 */

public interface FileCompressor {

    String EXTENSION_GZIP = ".gz";
    String EXTENSION_TAR = ".tar";

    File gzip(File source, String outputDir);

    File tar(File inDirectory, String outputFileAbsolutePath) throws IOException, ArchiveException;

}
