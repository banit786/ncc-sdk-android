package com.agero.nccsdk.internal.data.preferences;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccExceptionErrorCode;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccAccelerometerConfig;
import com.agero.nccsdk.domain.config.NccBarometerConfig;
import com.agero.nccsdk.domain.config.NccBatteryConfig;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.config.NccGyroscopeConfig;
import com.agero.nccsdk.domain.config.NccGyroscopeUncalibratedConfig;
import com.agero.nccsdk.domain.config.NccLinearAccelerometerConfig;
import com.agero.nccsdk.domain.config.NccLocationConfig;
import com.agero.nccsdk.domain.config.NccLocationModeConfig;
import com.agero.nccsdk.domain.config.NccMagneticFieldConfig;
import com.agero.nccsdk.domain.config.NccMagneticFieldUncalibratedConfig;
import com.agero.nccsdk.domain.config.NccMotionConfig;
import com.agero.nccsdk.domain.config.NccPhoneCallsConfig;
import com.agero.nccsdk.domain.config.NccRotationVectorConfig;
import com.agero.nccsdk.domain.config.NccWifiConfig;
import com.agero.nccsdk.internal.common.statemachine.SdkConfig;
import com.agero.nccsdk.internal.common.statemachine.SdkConfigType;
import com.agero.nccsdk.lbt.config.KinesisConfig;
import com.agero.nccsdk.adt.config.SessionConfig;
import com.agero.nccsdk.internal.log.config.LogConfig;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.google.gson.Gson;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.agero.nccsdk.internal.log.Timber;

/**
 * Created by james hermida on 9/8/17.
 */

@Singleton
public class NccConfigManager implements ConfigManager {

    private final String TAG = NccConfigManager.class.getSimpleName();
    private final Gson gson;

    private final NccSharedPrefs sharedPrefs;

    @Inject
    NccConfigManager(NccSharedPrefs sharedPrefs) {
        this.gson = new Gson();
        this.sharedPrefs = sharedPrefs;
    }

    @Override
    public void saveConfig(final NccSensorType sensorType, final NccConfig config) {
        if (config == null) {
            Timber.e("Cannot set a null config %s", sensorType.getName());
            return;
        }

        // TODO run on the executor?
        sharedPrefs.put(sensorType.getName(), gson.toJson(config));
        // TODO log
    }

    @Override
    public NccConfig getConfig(NccSensorType sensorType) throws NccException {
        NccConfig resultConfig;
        String cachedConfigString = sharedPrefs.getString(sensorType.getName());
        switch (sensorType) {
            // Google Play Services sensors
            case LOCATION:
                resultConfig = getConfigInstance(cachedConfigString, NccLocationConfig.class);
                break;
            case MOTION_ACTIVITY:
                resultConfig = getConfigInstance(cachedConfigString, NccMotionConfig.class);
                break;

            // Native sensors
            case ACCELEROMETER:
                resultConfig = getConfigInstance(cachedConfigString, NccAccelerometerConfig.class);
                break;
            case LINEAR_ACCELEROMETER:
                resultConfig = getConfigInstance(cachedConfigString, NccLinearAccelerometerConfig.class);
                break;
            case GYROSCOPE:
                resultConfig = getConfigInstance(cachedConfigString, NccGyroscopeConfig.class);
                break;
            case GYROSCOPE_UNCALIBRATED:
                resultConfig = getConfigInstance(cachedConfigString, NccGyroscopeUncalibratedConfig.class);
                break;
            case MAGNETIC_FIELD:
                resultConfig = getConfigInstance(cachedConfigString, NccMagneticFieldConfig.class);
                break;
            case MAGNETIC_FIELD_UNCALIBRATED:
                resultConfig = getConfigInstance(cachedConfigString, NccMagneticFieldUncalibratedConfig.class);
                break;
            case ROTATION_VECTOR:
                resultConfig = getConfigInstance(cachedConfigString, NccRotationVectorConfig.class);
                break;
            case BAROMETER:
                resultConfig = getConfigInstance(cachedConfigString, NccBarometerConfig.class);
                break;

                // Other
            case BATTERY:
                resultConfig = getConfigInstance(cachedConfigString, NccBatteryConfig.class);
                break;
            case LOCATION_MODE:
                resultConfig = getConfigInstance(cachedConfigString, NccLocationModeConfig.class);
                break;
            case WIFI:
                resultConfig = getConfigInstance(cachedConfigString, NccWifiConfig.class);
                break;
            case PHONE_CALLS:
                resultConfig = getConfigInstance(cachedConfigString, NccPhoneCallsConfig.class);
                break;
            default:
                throw new NccException(TAG, "Unknown sensor type '" + sensorType.getName() + "'", NccExceptionErrorCode.UNKNOWN_ERROR);
        }

        return resultConfig;
    }

    @Override
    public void saveSdkConfig(SdkConfigType configType, SdkConfig config) {
        if (config == null) {
            Timber.e("Cannot set a null SDK config for %s", configType.getName());
            return;
        }

        sharedPrefs.put(configType.getName(), gson.toJson(config));
    }

    @Override
    public SdkConfig getSdkConfig(SdkConfigType configType) throws NccException {
        SdkConfig resultConfig;
        String cachedConfigString = sharedPrefs.getString(configType.getName());
        switch (configType) {
            case KINESIS:
                resultConfig = getConfigInstance(cachedConfigString, KinesisConfig.class);
                break;
            case SESSION:
                resultConfig = getConfigInstance(cachedConfigString, SessionConfig.class);
                break;
            case LOG:
                resultConfig = getConfigInstance(cachedConfigString, LogConfig.class);
                break;
            default:
                throw new NccException(TAG, "Unknown config type '" + configType.getName() + "'", NccExceptionErrorCode.UNKNOWN_ERROR);
        }

        return resultConfig;
    }

    @SuppressWarnings("TryWithIdenticalCatches")
    private <T> T getConfigInstance(String cachedConfigString, Class<T> clazz) {
        try {
            if (StringUtils.isNullOrEmpty(cachedConfigString)) {
                Constructor<T> constructor = clazz.getConstructor();
                Timber.v("Getting default config %s", clazz.getSimpleName());
                return constructor.newInstance();
            } else {
                Timber.v("Getting cached config %s", clazz.getSimpleName());
                return gson.fromJson(cachedConfigString, clazz);
            }
        } catch (IllegalAccessException iae) {
            Timber.e(iae, "Exception getting config instance %s", clazz.getSimpleName());
        } catch (InstantiationException ie) {
            Timber.e(ie, "Exception getting config instance %s", clazz.getSimpleName());
        } catch (NoSuchMethodException nsme) {
            Timber.e(nsme, "Exception getting config instance %s", clazz.getSimpleName());
        } catch (InvocationTargetException ite) {
            Timber.e(ite, "Exception getting config instance %s", clazz.getSimpleName());
        }

        return null;
    }
}
