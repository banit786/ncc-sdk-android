package com.agero.nccsdk.internal.auth;

import android.content.Context;
import android.util.Log;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccExceptionErrorCode;
import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.internal.common.util.AppUtils;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.data.network.rest.apigee.model.ApigeeResponse;
import com.agero.nccsdk.internal.data.network.rest.apigee.ApigeeService;
import com.agero.nccsdk.internal.data.network.rest.apigee.model.AuthUserRequest;

import org.reactivestreams.Publisher;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class NccAuthManager implements AuthManager {

    private Context context;
    private DataManager dataManager;
    private ApigeeService apigeeService;
    private String accessId;

    private final int MAX_RETRIES = 6;
    private final int RETRY_DELAY = 10000; // 10 seconds
    private int retryCount = 0;

    @Inject
    public NccAuthManager(Context context, DataManager dataManager, ApigeeService apigeeService) {
        this.context = context;
        this.dataManager = dataManager;
        this.apigeeService = apigeeService;
    }

    @Override
    public Completable authenticate(String apiKey) {
        return Completable.create(emitter -> {
            if (hasAuthenticatedWithApiKey(apiKey)) {
                emitter.onComplete();
            } else {
                final AuthUserRequest body = new AuthUserRequest(AppUtils.getAppPackage(context));
                final String unauthorizedMessage = "Package '" + body.getAppId() + "' is not authorized with API key '" + apiKey + "'";
                apigeeService.authenticate(apiKey, body)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe(disposable -> {
                            if (retryCount > 0) {
                                Log.e(NccSdk.class.getSimpleName(), "Retrying SDK authentication... ");
                            }
                        })
                        .retryWhen(throwableFlowable -> throwableFlowable.flatMap(
                                (Function<Throwable, Publisher<?>>) throwable -> {
                                    // Attempt to retry if a non-401 error occurs
                                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == HTTP_UNAUTHORIZED) {
                                        return Flowable.error(getErrorException(unauthorizedMessage));
                                    } else if (++retryCount <= MAX_RETRIES) {
                                        return Flowable.timer(RETRY_DELAY, TimeUnit.MILLISECONDS);
                                    }
                                    return Flowable.error(throwable);
                                }
                        ))
                        .subscribe(new DisposableSingleObserver<ApigeeResponse>() {
                            @Override
                            public void onSuccess(ApigeeResponse apigeeResponse) {
                                if (apigeeResponse.getCode() == HTTP_OK) {
                                    if (StringUtils.isNullOrEmpty(apigeeResponse.getIdentityPoolId())) {
                                        emitter.onError(getErrorException("Internal error occurred."));
                                    } else {
                                        accessId = apigeeResponse.getIdentityPoolId();
                                        dataManager.setAuthenticatedApiKey(apiKey);
                                        dataManager.setAccessId(accessId);
                                        emitter.onComplete();
                                    }
                                } else {
                                    emitter.onError(getErrorException( "Unhandled code " + apigeeResponse.getCode()));
                                }
                            }

                            @Override
                            public void onError(Throwable t) {
                                retryCount = 0;
                                emitter.onError(t);
                            }
                        });
            }
        });
    }

    @Override
    public Single<Boolean> hasAuthenticated() {
        return Single.create(emitter -> emitter.onSuccess(!StringUtils.isNullOrEmpty(accessId) || !StringUtils.isNullOrEmpty(dataManager.getAuthenticatedApiKey())));
    }

    private boolean hasAuthenticatedWithApiKey(String apiKey) {
        String key = dataManager.getAuthenticatedApiKey();
        return !StringUtils.isNullOrEmpty(key) && key.equals(apiKey);
    }

    private NccException getErrorException(String message) {
        return new NccException(NccSdk.class.getSimpleName(), "Failed to authenticate SDK. " + message, NccExceptionErrorCode.INITIALIZE_ERROR);
    }
}
