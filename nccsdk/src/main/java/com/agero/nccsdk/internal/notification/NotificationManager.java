package com.agero.nccsdk.internal.notification;

import android.support.annotation.DrawableRes;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.internal.notification.model.NccNotification;

/**
 * Created by james hermida on 12/8/17.
 */

public interface NotificationManager {

    void setNotification(int id, @DrawableRes int drawableId, String contentTitle, String contextText) throws NccException;
    NccNotification getNotification(int id);

}
