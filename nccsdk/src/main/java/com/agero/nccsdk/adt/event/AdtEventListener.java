package com.agero.nccsdk.adt.event;

/**
 * Interface definition for a callback to be invoked when a driving event occurs.
 * These methods are invoked on the main thread.
 */
public interface AdtEventListener {

    /**
     * Invoked when a driving start event has occurred
     */
    void onDrivingStart();

    /**
     * Invoked when a driving stop event has occurred
     */
    void onDrivingStop();

}
