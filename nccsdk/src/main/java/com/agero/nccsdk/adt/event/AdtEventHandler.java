package com.agero.nccsdk.adt.event;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.adt.AdtState;
import com.agero.nccsdk.adt.AdtStateMachine;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.agero.nccsdk.internal.log.Timber;

import java.util.concurrent.CopyOnWriteArrayList;

import javax.inject.Singleton;

@Singleton
public class AdtEventHandler extends BroadcastReceiver {

    public static final String ACTION_SESSION_START = "com.agero.nccsdk.ACTION_SESSION_START";
    public static final String ACTION_SESSION_END = "com.agero.nccsdk.ACTION_SESSION_END";

    private final CopyOnWriteArrayList<AdtEventListener> listeners = new CopyOnWriteArrayList<>();

    private final StateMachine<AdtState> adtStateStateMachine;

    public AdtEventHandler(StateMachine<AdtState> adtStateStateMachine) {
        this.adtStateStateMachine = adtStateStateMachine;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (StringUtils.isNullOrEmpty(action)) {
            Timber.e("%s cannot process broadcast: missing action from intent", AdtEventHandler.class.getSimpleName());
        } else {
            if (action.equalsIgnoreCase(ACTION_SESSION_START)
                    || action.equalsIgnoreCase(ACTION_SESSION_END)) {
                notifyListeners(action);
            } else {
                Timber.e("Expected action [%s | %s]. Actual %s", ACTION_SESSION_START, ACTION_SESSION_END, action);
            }
        }
    }

    public boolean registerListener(AdtEventListener listener) {
        if (listener != null) {
            if (listeners.contains(listener)) {
                Timber.w("%s has already been registered for ADT events", getClassNameAndHashCode(listener));
            } else {
                listeners.add(listener);
                Timber.d("Registered %s for ADT events", getClassNameAndHashCode(listener));
                printListeners();
            }
        } else {
            Timber.w("Attempted to register for ADT events with a null listener");
        }
        return ((AdtStateMachine) adtStateStateMachine).isDriveInProgress();
    }

    public void unregisterListener(AdtEventListener listener) {
        if (listeners.contains(listener)) {
            listeners.remove(listener);
            Timber.d("Unregistered %s for ADT events", getClassNameAndHashCode(listener));
            printListeners();
        } else {
            Timber.w("%s has not been registered for ADT events", getClassNameAndHashCode(listener));
        }
    }

    private void notifyListeners(final String action) {
        if (listeners != null) {
            for (AdtEventListener listener : listeners) {
                if (action.equalsIgnoreCase(ACTION_SESSION_START)) {
                    listener.onDrivingStart();
                } else if (action.equalsIgnoreCase(ACTION_SESSION_END)) {
                    listener.onDrivingStop();
                }
            }
        }
    }

    private String getClassNameAndHashCode(AdtEventListener listener) {
        if (listener != null) {
            return listener.getClass().getName() + "@" + listener.hashCode();
        }

        return "";
    }

    private void printListeners() {
        if (listeners != null) {
            StringBuilder log = new StringBuilder("Current ADT listeners: ");
            for (int i = 0; i < listeners.size(); i++) {
                log.append("(").append((i + 1)).append(") ").append(getClassNameAndHashCode(listeners.get(i))).append(" ");
            }
            Timber.d(log.toString());
        }
    }
}
